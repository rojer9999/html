const $ = require('jquery');
import 'owl.carousel';

(function($) {
  $(function() {

    // Tabs
    $('ul.tabs').on('click', 'li:not(.active)', function() {
      $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('#tabs').find('div.tab-content').removeClass('active').eq($(this).index()).addClass('active');
    });

    // Accordion
    $('.accordion').on('click', '.accordion-header', function() {
      $(this).toggleClass('active').next().toggleClass('active');
    });

    // Circle bar
    $('.inner-bf').each((i, el) => {
      const deg = el.getAttribute('data-deg') * 3.6;
      const circleAccent = '#fa3f40';
      const circleWhite = '#ffffff';

      if (deg > 180) {
        el.style.backgroundColor = circleAccent;
        el.style.transform = `rotate(${deg}deg)`;
      } else {
        el.style.backgroundColor = circleWhite;
        el.style.transform = `rotate(${+deg - 180}deg)`;
      }
    });

    // Slider Providers
    $('#providers-carousel').owlCarousel({
      loop: true,
      margin: 30,
      smartSpeed: 900,
      items: 1,
      responsive: {
        640: {
          items: 2
        },
        992: {
          margin: 30,
          items: 2
        },
        1200: {
          margin: 80,
          items: 2
        }
      }
    });

    // Slider Testimonials
    $('#testimonials-carousel').owlCarousel({
      loop: true,
      margin: 30,
      smartSpeed: 900,
      items: 1,
      dots: true,
      dotsContainer: '#carousel-custom-dots'
    });

    let prevScroll;
    $(window).scroll(function(){
      const sticky = $('.sticky');
      const curScroll = $(window).scrollTop();
      if (curScroll >= 500 && prevScroll > curScroll){
        sticky.addClass('fixed');
      } else {
        sticky.removeClass('fixed');
      }
      prevScroll = curScroll;
    });
    
    $('a[href^="#"]').on('click', function(event) {
      const target = $(this.getAttribute('href'));
      if( target.length ) {
          event.preventDefault();
          $('html, body').stop().animate({
              scrollTop: target.offset().top
          }, 1000);
        }
    });
      
  });
})($);